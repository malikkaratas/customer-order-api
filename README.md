# Customer Api docker containers

A Proof-of-concept of a running Customer api application inside containers

```
git clone https://bitbucket.org/malikkaratas/customer-order-api.git 

cd docker

docker-compose up

```

### PHP (PHP-FPM) - Build App

Composer is included

```


cd customer-api
cp env.dist .env

--docker exec -it [image_name] sh

php bin/console doctrine:database:create

php bin/console doctrine:schema:update --force

php bin/console doctrine:fixtures:load >>  Careful, database "abc_db" will be purged. Do you want to continue? (yes/no) [no]: yes

NOT: Ben şimdilik kendi olusturduğum public-private rsa'ları koydum gite, kolayca build edebilesiniz diye. Bunu yapmamımın güvenlik açıgı oldugunu biliyorum sadece kolay build edebilseniz diye böyle de sunayım dedim.

--Opsiyonel  
openssl genrsa -out config/jwt/private.pem -aes256 4096 
openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem

.env dosyasında JWT_PASSPHRASE keyine girdiğiniz şifreyi size ait public ve private rsa keyler olusacaktır.

```

### Unit Test

```
php ./bin/phpunit
php bin/phpunit tests/AuthControllerTest
php bin/phpunit tests/OrderControllerTest

```

### POSTMAN Collection/env

postman collection url: https://www.postman.com/collections/6fd926c0449ee855a6f8
postman collection enviroment json: https://yadi.sk/d/EyWgKf6sVDhZOA

