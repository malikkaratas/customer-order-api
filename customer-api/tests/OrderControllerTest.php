<?php


namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OrderControllerTest extends WebTestCase
{

    /** @test */
    public function isReturnGetOrders200Status()
    {
        $client = static::createClient();
        $client->request('GET','/api/orders?limit=5&page=2',[],[],
            ['headers' =>['Authorization' =>
                'Bearer [TOKEN]']]
        );
        //$this->assertTrue($client->getResponse()->isOk());
        $this->assertJson($client->getResponse()->getContent());
    }

    /** @test */
    public function isReturnGetOrder200Status()
    {
        $client = static::createClient();
        $client->request('GET','/api/order/1',[],[],
            ['Authorization' =>
                'Bearer [TOKEN]']
        );
        //$this->assertTrue($client->getResponse()->isOk());
        $this->assertJson($client->getResponse()->getContent());
    }

    /** @test */
    public function isReturnPostOrder200Status()
    {
        $client = static::createClient();
        $client->request('POST','/api/order',[],[],
            [ 'Authorization' =>
        ['Bearer' =>  '[TOKEN]']]
        );
        //$this->assertTrue($client->getResponse()->isOk());
        $this->assertJson($client->getResponse()->getContent());
    }

    /** @test */
    public function isReturnPutOrder200Status()
    {
        $client = static::createClient();
        $client->request('PUT','/api/order/1',[],[],
            [ 'Authorization' =>
        ['Bearer' =>  '[TOKEN]']]
        );
        //$this->assertTrue($client->getResponse()->isOk());
        $this->assertJson($client->getResponse()->getContent());
    }

}