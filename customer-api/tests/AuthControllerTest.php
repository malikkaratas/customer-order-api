<?php


namespace App\Tests;

use Faker;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthControllerTest extends WebTestCase
{

    /** @test */
    public function isRegisterReturn200Status()
    {
        $faker = Faker\Factory::create();
        $client = $this->createClient();
        $jsonData = json_encode(['username' => $faker->userName, 'password' => $faker->password],true);
        $client->request(
            'POST',
            '/register',[],[],[],$jsonData
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}