<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder =$passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        // create 20 user! Bam!
        for ($i = 0; $i < 10; $i++) {
            $customer = new Customer('customer'.($i+1));
            $customer->setPassword($this->passwordEncoder->encodePassword($customer,'123456'));
            $manager->persist($customer);
        }

        for ($i = 0; $i < 10; $i++) {
            $product = new Product();
            $product->setName('product'.$i);
            $product->setCode('product-code-'.$i);
            $manager->persist($product);
        }

        $manager->flush();

        /** @var Product[] $products */
        $products = $manager->getRepository(Product::class)->findAll();

        /** @var Customer[] $customers */
        $customers = $manager->getRepository(Customer::class)->findAll();

        for ($i = 0; $i < 10; $i++) {
            $order = new Order();

            $shippingDate = date_create_from_format('Y-m-d H:i:s', '2020-12-12 15:00:00');
            $shippingDate->getTimestamp();
            $order->setCustomer($customers[$i])->setProduct($products[$i])->setAddress($faker->address)->
                setOrderCode('code'.$i)->setQuantity($i+1)->setShippingDate($shippingDate);
            $manager->persist($order);
        }
        $manager->flush();
    }
}
