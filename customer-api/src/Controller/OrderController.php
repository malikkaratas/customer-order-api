<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\Product;
use App\Repository\OrderRepository;
use Doctrine\ORM\QueryBuilder;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class OrderController
 * @package App\Controller
 *
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/order", name="post_order", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function postOrder(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $rawData = json_decode($request->getContent(),true);

        /** @var OrderRepository $OrderRepo */
        $orderRepo = $em->getRepository(Order::class);

        /** @var Customer|null $customer */
        $customer = $em->getRepository(Customer::class)->findOneBy(['id' => $rawData['customer']]);
        if (is_null($customer)){
            return new JsonResponse(['message' => sprintf('Customer %s not found', $rawData['customer'])],Response::HTTP_NOT_FOUND);
        }

        /** @var Product|null $product */
        $product = $em->getRepository(Product::class)->findOneBy(['id' => $rawData['product']]);
        if (is_null($product)){
            return new JsonResponse(['message' => sprintf('Product %s not found', $rawData['product'])],Response::HTTP_NOT_FOUND);
        }

        $order = new Order();

        $shippingDate = date_create_from_format('Y-m-d H:i:s', $rawData['shippingDate']);
        $shippingDate->getTimestamp();

        $order->setCustomer($customer)->setProduct($product)->setAddress($rawData['address'])->
        setOrderCode($rawData['orderCode'])->setQuantity($rawData['quantity'])
            ->setShippingDate($shippingDate);

        $em->persist($order);
        $em->flush();

        return new JsonResponse(['message'=> sprintf('Order %s successfully created', $order->getId()), 'set' => $order->getId()],Response::HTTP_OK);
    }

    /**
     * @Route("/order/{id}", name="update_order", methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function updateOrder($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $rawData = json_decode($request->getContent(),true);

        /** @var OrderRepository $OrderRepo */
        $orderRepo = $em->getRepository(Order::class);

        $filters = [
            'orderStatus' => ['w1'],
            'id' => $id
        ];

        /** @var Order|null $order */
        $order = $orderRepo->lastShippingOrder($filters);
        
        if (is_null($order)){
            return new JsonResponse(['message' => sprintf('Order %s not found', $rawData['customer'])],Response::HTTP_NOT_FOUND);
        }

        /** @var Customer|null $customer */
        $customer = $em->getRepository(Customer::class)->findOneBy(['id' => $rawData['customer']]);
        if (is_null($customer)){
            return new JsonResponse(['message' => sprintf('Customer %s not found', $rawData['customer'])],Response::HTTP_NOT_FOUND);
        }

        /** @var Product|null $product */
        $product = $em->getRepository(Product::class)->findOneBy(['id' => $rawData['product']]);
        if (is_null($product)){
            return new JsonResponse(['message' => sprintf('Product %s not found', $rawData['product'])],Response::HTTP_NOT_FOUND);
        }


        $shippingDate = date_create_from_format('Y-m-d H:i:s', $rawData['shippingDate']);
        $shippingDate->getTimestamp();

        $order->setCustomer($customer)->setProduct($product)->setAddress($rawData['address'])->setOrderStatus($rawData['orderStatus'])
            ->setOrderCode($rawData['orderCode'])->setQuantity($rawData['quantity'])->setShippingDate($shippingDate);

        $em->persist($order);
        $em->flush();

        return new JsonResponse(['message'=> sprintf('Order %s successfully updated', $order->getId()), 'set' => $order->getId()],Response::HTTP_OK);
    }


    /**
     * @Route("/order/{id}", name="get_order", methods={"GET"})
     * @param Request $request
     * @param $id
     */
    public function getOrder($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Order|null $order */
        $order = $em->getRepository(Order::class)->findOneBy(['id' => $id]);
        if (is_null($order)){
            return new JsonResponse(['message' => sprintf('Order %s not found', $id)],Response::HTTP_NOT_FOUND);
        }
        $response = $this->buildSerializer(['set' => $order]);
        return new JsonResponse($response, Response::HTTP_OK);
    }

    /**
     * @Route("/orders", name="get_orders", methods={"GET"})
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return JsonResponse
     */
    public function getOrders(Request $request, PaginatorInterface $paginator)
    {
        $em = $this->getDoctrine()->getManager();
        $filters = [
            'limit' => $request->request->get('limit') ?? 3,
            'page' => $request->request->get('page') ?? 1
        ];


        /** @var OrderRepository $orderRepo */
        $orderRepo = $em->getRepository(Order::class);

        $orderQb = $orderRepo->getOrders();
        $pagination = $paginator->paginate(
            $orderQb, /* query NOT result */
            $request->query->getInt('page', $filters['page'])/*page number*/,
            $filters['limit']/*limit per page*/
        );

        $orders = $orderQb->getResult();
        if (empty($orders)){
            return new JsonResponse(['message' => sprintf('Orders not found')],Response::HTTP_NOT_FOUND);
        }

        $response = $this->buildSerializer(['set' => $pagination]);
        return new JsonResponse($response, Response::HTTP_OK);
    }

    private function buildSerializer($data, $contextGroups = [])
    {

        $serializer = SerializerBuilder::create();
        $serializer->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy());
        $serializer = $serializer->build();

        return $serializer->toArray($data);
    }

}
