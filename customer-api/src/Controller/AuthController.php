<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Customer;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    /**
     * @Route("/register", name="post_register", methods={"POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $rawData = json_decode($request->getContent(),true);

        $customer = new Customer($rawData['username']);
        $customer->setPassword($encoder->encodePassword($customer, $rawData['password']));
        try {
            $em->persist($customer);
            $em->flush();
        } catch (\Exception $exception){
            return new JsonResponse(['set' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse(['message'=>'Customer successfully created', 'set' => $customer->getUsername()], Response::HTTP_OK);
    }

    public function api()
    {
        return new JsonResponse($this->getUser()->getUsername(),Response::HTTP_OK);
    }
}
